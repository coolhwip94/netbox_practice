from netbox import NetBox


netbox = NetBox(host='demo.netbox.dev', port=443, use_ssl=True, auth_token='e15f4660048382c86d9c61ea01752caacc023af8')

devices = netbox.dcim.get_devices()
device_roles = netbox.dcim.get_device_roles()