from dotenv import load_dotenv
import os
from netbox import NetBox

load_dotenv()

# Local Environment
netbox_auth_token = os.getenv('netbox_auth_token')
netbox = NetBox(host='192.168.1.136', port=8000, use_ssl=False, auth_token=netbox_auth_token)


# Netbox Demo Environment Publicly Available : https://demo.netbox.dev
netbox_demo_token = "f18e0a3b5886af484a2c9e4a4f0b3fe6e751585d"  # token obtained from public demo env
netboxdemo = NetBox(host='demo.netbox.dev', port=443, use_ssl=True, auth_token=netbox_auth_token)

# Get all devices in netbox, returns a list/array
devices = netboxdemo.dcim.get_devices()

''' Example of one device item in return
{'id': 102,
 'url': 'https://demo.netbox.dev/api/dcim/devices/102/',
 'display': 'B97 R135a <-> B97 R237',
 'name': 'B97 R135a <-> B97 R237',
 'device_type': {'id': 11,
  'url': 'https://demo.netbox.dev/api/dcim/device-types/11/',
  'display': '48-Pair Fiber Panel',
  'manufacturer': {'id': 13,
   'url': 'https://demo.netbox.dev/api/dcim/manufacturers/13/',
   'display': 'Generic',
   'name': 'Generic',
   'slug': 'generic'},
  'model': '48-Pair Fiber Panel',
  'slug': '48-pair-fiber-panel'},
 'device_role': {'id': 6,
  'url': 'https://demo.netbox.dev/api/dcim/device-roles/6/',
  'display': 'Patch Panel',
  'name': 'Patch Panel',
  'slug': 'patch-panel'},
 'tenant': None,
 'platform': None,
 'serial': '',
 'asset_tag': None,
 'site': {'id': 25,
  'url': 'https://demo.netbox.dev/api/dcim/sites/25/',
  'display': 'Werk 1',
  'name': 'Werk 1',
  'slug': 'werk-1'},
 'location': {'id': 6,
  'url': 'https://demo.netbox.dev/api/dcim/locations/6/',
  'display': 'R135a',
  'name': 'R135a',
  'slug': 'r135a',
  '_depth': 0},
 'rack': {'id': 43,
  'url': 'https://demo.netbox.dev/api/dcim/racks/43/',
  'display': 'S3',
  'name': 'S3'},
 'position': 6,
 'face': {'value': 'front', 'label': 'Front'},
 'parent_device': None,
 'status': {'value': 'active', 'label': 'Active'},
 'primary_ip': None,
 'primary_ip4': None,
 'primary_ip6': None,
 'cluster': None,
 'virtual_chassis': None,
 'vc_position': None,
 'vc_priority': None,
 'comments': '',
 'local_context_data': None,
 'tags': [],
 'custom_fields': {},
 'config_context': {'ntp-servers': ['172.16.10.22', '172.16.10.33'],
  'syslog-servers': ['172.16.9.100', '172.16.9.101']},
 'created': '2021-10-11',
 'last_updated': '2021-10-11T08:41:50.022296Z'}
'''
 
# Get devices using filters
netboxdemo.dcim.get_devices(rack_id=1)  # by rack id
netboxdemo.dcim.get_devices(name='device_name')  # by device name
