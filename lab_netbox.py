from dotenv import load_dotenv
import os
from netbox import NetBox

def get_unreachable_ipv4(ipv4_list=[]) -> list:
    '''
    This function intakes a list of ipv4 addresses without cidr and performs a ping to check for reachability
    and returns any ipv4 addreses that do not receive a response    
    '''
    unused_ipv4 = []

    for ipv4 in ipv4_list:
        response = os.system('ping -c 1 -W 2 ' + ipv4)

        if response == 0:
            print(f'{ipv4} is in use')
        else:
            print(f'{ipv4} is available')
            unused_ipv4.append(ipv4)

    return unused_ipv4



if __name__ == '__main__':

    load_dotenv()

    # Connect to netbox in the lab
    netbox_auth_token = os.getenv('netbox_auth_token')

    netbox = NetBox(host='10.11.4.48', port=443, use_ssl=True, auth_token=netbox_auth_token)


    # grab devices from netbox
    devices = netbox.dcim.get_devices()

    # grab ip addresses from netbox
    ip_addresses = netbox.ipam.get_ip_addresses()


    # Grab just the address from response, no cidr
    ipv4_addresses = [address['address'].split('/')[0] for address in ip_addresses]

    unused_ipv4 = get_unreachable_ipv4(ipv4_addresses)

    # Output unused IPv4 addresses 
    print(unused_ipv4)


    # Creating a new IP address
    ip_address_payload = {
    'family': {'value': 4, 'label': 'IPv4'},
    'description': 'test ltruong',
    'tags': [],
    }
    # netbox.ipam.create_ip_address(address="192.168.1.6", **ip_address_payload)

