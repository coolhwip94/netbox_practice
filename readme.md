# Netbox Practice
> This repo is intended for examples and familiarization with netbox interactions from python

## Setup
---
### Python:
1. Install requirements (main thing you need is python-netbox)
    ```
    pip3 install -r requirements.txt
    ```
2. Run command to view example output
    ```
    python3 main.py
    ```
** Note : You dont need python-netbox, you can make API calls into netbox just using the python requests module)

### Environment:
1. Create a `.env` file and populate with netbox auth token
```
netbox_auth_token="abcd123"
```


## References: 
---
Setup : https://github.com/netbox-community/netbox-docker
Netbox API Demo : https://demo.netbox.dev/api/
Python Netbox Docs : https://python-netbox.readthedocs.io/en/latest/index.html#